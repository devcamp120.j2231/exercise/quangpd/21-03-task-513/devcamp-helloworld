import image from "./assets/images/pasted-image-0.png"

const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: image,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100,
    countPercentStudyingStudent(){
      return this.studyingStudents / this.totalStudents * 100;
    }
  }

  export {
    gDevcampReact
  }