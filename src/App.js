import {gDevcampReact} from "./data";
 
function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src = {gDevcampReact.image} width={500} alt ="Title"/>
      <p>Tỷ lệ sinh viên đang theo học : {gDevcampReact.countPercentStudyingStudent()} %</p>
      <p>{gDevcampReact.countPercentStudyingStudent() > 15 ? "Sinh viên đăng ký học nhiều" :  "Sinh Viên Đăng Ký Học ít"}</p>
      <ul>
        {gDevcampReact.benefits.map((item ,index) =>{
          return <li key ={index}>{item}</li>
        })}
      </ul>
    </div>
    
  );
}

export default App;
